import React, {useState, useEffect} from 'react';
import {StyleSheet, Text, View, TouchableOpacity, ImageBackground} from 'react-native';
import { Camera } from 'expo-camera';

export default function App() {
    const [hasPermission, setHasPermission] = useState(null);
    const [type, setType] = useState(Camera.Constants.Type.back);

    useEffect(() => {
        (async () => {
            const { status } = await Camera.requestPermissionsAsync();
            setHasPermission(status === 'granted');
        })();
    }, []);

    if (hasPermission === null) {
        return <View />;
    }
    if (hasPermission === false) {
        return <Text>No access to camera</Text>;
    }
  return (
      <View style={styles.main}>
          <Camera style={{ flex: 1 }} type={type}>
          </Camera>
          <ImageBackground source={require('./assets/img.jpg')} style={styles.image}>
              <View
                  style={styles.container}>
                  <Text style={styles.heading}>WTF!</Text>
                  <Text style={styles.heading}> </Text>
                  <Text style={styles.heading}> </Text>
                  <Text style={styles.heading}> </Text>
                  <Text style={styles.heading}> </Text>
              </View>
          </ImageBackground>
      </View>
  );
}

const styles = StyleSheet.create({
    main: {
      flexDirection: 'row',
        flex: 1
    },
  container: {
    flex: 1,
    backgroundColor: '#fefefe',
    alignItems: 'center',
    justifyContent: 'center',
      backgroundColor: 'transparent',
      flexDirection: 'column'
  },
  heading: {
      color: '#fefefe',
    fontSize: 12,
    fontWeight: 'bold'
  },
    image: {
      flex: 1,
      width: '100%'
    }
});
